import Link from 'next/link';

export default function nav() {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-warning">
            <div className="container">
                <Link href="/"><a className="navbar-brand">CryptoApp</a></Link>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link href="/"><a className="nav-link">Home</a></Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/about"><a className="nav-link">About</a></Link>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
    )
}
