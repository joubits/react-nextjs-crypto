import MasterPage from '../components/Master';
import fetch from 'isomorphic-unfetch';

const Index = (props) => (
    <MasterPage>
        <div className="row">
            <div className="col-12">
                <h2>BitCoin Price</h2>
                { console.log(props) }
            </div>
            <div className="col-md-8">
                <h2>BitCoin News</h2>
            </div>
            <div className="col-md-4">
                <h2>Next BitCoin Events</h2>
            </div>
        </div>
    </MasterPage>
)

Index.getInitialProps = async () => {
    const price = await fetch('https://api.coinmarketcap.com/v2/ticker/1');

    const resp = await price.json();

    return {
        bitcoinPrice: resp.data.quotes.USD
    }
}

export default Index;